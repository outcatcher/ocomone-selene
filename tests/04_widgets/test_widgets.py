# -*- coding=utf-8 -*-

from ocomone import Resources
from selene import browser
from selene.conditions import hidden, visible

from ocomone_selene import WiredDecorator
from ocomone_selene.elements import Button
from ocomone_selene.widgets import BaseWidget

wired = WiredDecorator(Resources(__file__, "."))


@wired("widget.yml")
class Footer(BaseWidget):
    report_error: Button  # css: a.w3-button[onclick*=displayError]
    print_page: Button  # css: a.w3-button[onclick*=printPage]
    forum: Button  # css: a.w3-button[href*="/forum/"]


URL = "https://www.w3schools.com/cert/default.asp"

def _footer():
    return Footer(browser.element("#footer"))

def test_widgets_basic(start_stop):
    browser.open_url(URL)
    footer = _footer()
    assert footer.report_error.text == "REPORT ERROR"
    assert footer.print_page.text == "PRINT PAGE"
    assert footer.forum.text == "FORUM"


def test_widgets_assure(start_stop):
    browser.open_url(URL)
    footer = _footer()
    footer.assure(visible)
    footer.assure_not(hidden)
    assert footer.is_displayed()


def test_get_element(start_stop):
    browser.open_url(URL)
    footer = _footer()
    assert footer.get_element("forum", Button).text == "FORUM"
    assert footer.get_element("about", Button).text == "ABOUT"
