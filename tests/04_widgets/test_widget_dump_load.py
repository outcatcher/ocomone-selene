# -*- coding=utf-8 -*-
import pytest
import yaml
from ocomone import Resources
from selene import browser
from selene.conditions import visible

from ocomone_selene.elements import TextInput
from ocomone_selene.widgets import BaseWidget

resources = Resources(__file__, ".")

W3C = "https://www.w3schools.com/html/default.asp"


class YmlWidget(BaseWidget):
    email: TextInput  # css: input#err_email
    description: TextInput  # css: textarea#err_desc


def test_read_yml(start_stop):
    browser.open_url(W3C)
    browser.element("a.w3-button[onclick*=displayError]").click()
    root = browser.element("div#err_form")
    suggestion = YmlWidget(root)
    suggestion.root_element.scroll_to()
    suggestion.load(resources["sugg_data.yml"])
    assert suggestion.email.text == "email@email"
    assert suggestion.description.text == "My description"


class ParentWidget(BaseWidget):
    suggestion: YmlWidget  # css: div#err_form


def test_complex_yml(start_stop):
    browser.open_url(W3C)
    browser.element("a.w3-button[onclick*=displayError]").click()
    parent = ParentWidget()
    parent.suggestion.root_element.scroll_to()
    parent.load(resources["sugg_parent_data.yml"])
    assert parent.suggestion.email.text == "email@email"
    assert parent.suggestion.description.text == "My description"


def test_complex_yml_verify(start_stop):
    browser.open_url(W3C)
    browser.element("a.w3-button[onclick*=displayError]").click()
    parent = ParentWidget()
    parent.suggestion.root_element.scroll_to()
    data_file = resources["sugg_parent_data.yml"]
    parent.load(data_file)
    with open(data_file) as exp_file:
        expected_data = yaml.load(exp_file)
    parent.validate_fields(expected_data)


def test_yml_compare_widget(start_stop):
    browser.open_url(W3C)
    browser.element("a.w3-button[onclick*=displayError]").click()
    root = browser.element("div#err_form")
    root.assure(visible)
    suggestion = YmlWidget(root)
    suggestion.root_element.scroll_to()
    suggestion.load(resources["sugg_data.yml"])
    expected_data = {
        "email": "email@email",
        "description": "My description",
    }
    assert suggestion == expected_data


@pytest.mark.xfail(raises=AssertionError)
def test_yml_verify_negative(start_stop):
    browser.open_url(W3C)
    browser.element("a.w3-button[onclick*=displayError]").click()
    root = browser.element("div#err_form")
    root.assure(visible)
    suggestion = YmlWidget(root)
    suggestion.root_element.scroll_to()
    suggestion.load(resources["sugg_data.yml"])
    with open(resources["negative_sugg_data.yml"]) as exp_file:
        expected_data = yaml.load(exp_file)
    suggestion.validate_fields(expected_data)


def test_yml_verify_too_much_data(start_stop):
    browser.open_url(W3C)
    browser.element("a.w3-button[onclick*=displayError]").click()
    root = browser.element("div#err_form")
    root.assure(visible)
    suggestion = YmlWidget(root)
    suggestion.root_element.scroll_to()
    suggestion.load(resources["sugg_data.yml"])
    with open(resources["sugg_data_2_much.yml"]) as exp_file:
        expected_data = yaml.load(exp_file)
    suggestion.validate_fields(expected_data)


def test_yml_verify_partial(start_stop):
    browser.open_url(W3C)
    browser.element("a.w3-button[onclick*=displayError]").click()
    root = browser.element("div#err_form")
    root.assure(visible)
    suggestion = YmlWidget(root)
    suggestion.root_element.scroll_to()
    suggestion.load(resources["sugg_data.yml"])
    with open(resources["sugg_data_partial.yml"]) as exp_file:
        expected_data = yaml.load(exp_file)
    suggestion.validate_fields(expected_data)



def test_yml_verify_partial_with_none(start_stop):
    browser.open_url(W3C)
    browser.element("a.w3-button[onclick*=displayError]").click()
    root = browser.element("div#err_form")
    root.assure(visible)
    suggestion = YmlWidget(root)
    suggestion.root_element.scroll_to()
    suggestion.load(resources["sugg_data.yml"])
    with open(resources["sugg_data_partial_with_none.yml"]) as exp_file:
        expected_data = yaml.load(exp_file)
    suggestion.validate_fields(expected_data)


class VeryRoot(BaseWidget):
    parent: ParentWidget  # css: div#belowtopnav


def test_very_complex_yml(start_stop):
    browser.open_url(W3C)
    browser.element("a.w3-button[onclick*=displayError]").click()
    root = VeryRoot()
    root.parent.suggestion.root_element.scroll_to()
    root.load(resources["sugg_very_data.yml"])
    assert root.parent.suggestion.email.text == "email@email"
    assert root.parent.suggestion.description.text == "My description"


def test_dump_data(start_stop):
    description = "My description"
    email = "my@email.com"

    expected_data = {
        "description": description,
        "email": email
    }

    browser.open_url(W3C)
    browser.element("a.w3-button[onclick*=displayError]").click()
    root = browser.element("div#err_form")
    suggestion = YmlWidget(root)
    suggestion.email = email
    suggestion.description = description
    file_path = resources["tmp1.yml"]
    suggestion.dump(file_path)

    with open(file_path) as file:
        actual_data = yaml.load(file)
    assert expected_data == actual_data


def test_dump_complex_data(start_stop):
    description = "My description"
    email = "my@email.com"

    expected_data = {"suggestion": {
        "description": description,
        "email": email
    }}

    browser.open_url(W3C)
    browser.element("a.w3-button[onclick*=displayError]").click()
    parent = ParentWidget()
    parent.suggestion.email = email
    parent.suggestion.description = description
    file_path = resources["tmp2.yml"]
    parent.dump(file_path)

    with open(file_path) as file:
        actual_data = yaml.load(file)
    assert expected_data == actual_data
