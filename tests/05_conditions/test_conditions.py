# -*- coding=utf-8 -*-
import pytest
from selene import browser
from selenium.common.exceptions import TimeoutException

from ocomone_selene.conditions import get_condition
from ocomone_selene.elements import SeleneElementWrapper

URL = "https://www.w3schools.com/cert/default.asp"


def test_condition_conversion(start_stop):
    browser.open_url(URL)
    btn = browser.element(".w3-clear .w3-left")

    def _contains_text(elem: SeleneElementWrapper):
        return "Home" in elem.text

    btn.assure(get_condition(_contains_text))


@pytest.mark.xfail(raises=TimeoutException)
def test_condition_conversion_negative(start_stop):
    browser.open_url(URL)
    btn = browser.element(".w3-clear .w3-left")
    btn.assure(get_condition(lambda elem: "Home" in elem.text))
    btn.assure(get_condition(lambda elem: "Home" not in elem.text), timeout=1)


def test_wrapper_condition_conversion(start_stop):
    browser.open_url(URL)
    btn = SeleneElementWrapper(browser.element(".w3-clear .w3-left"))

    def _contains_text(elem: SeleneElementWrapper):
        return "Home" in elem.text

    btn.assure(_contains_text)
