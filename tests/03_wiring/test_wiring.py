from typing import List

import pytest
from ocomone import Resources
from selene import browser
from selene.elements import SeleneCollection, SeleneElement

from ocomone_selene import Wireable, WiredDecorator
from ocomone_selene.elements import Button, TextInput
from ocomone_selene.widgets import BaseWidget

wired = WiredDecorator(Resources(__file__, "."))


class WiredThing(Wireable):
    def elements(self, locator) -> SeleneCollection:
        return browser.elements(locator)

    def element(self, locator) -> SeleneElement:
        return browser.element(locator)


@wired("test.yml")
class WiredYml(WiredThing):
    button: SeleneElement
    input: TextInput


@wired("test.csv")
class WiredCsv(WiredThing):
    search: SeleneElement


@wired("test_comma.csv", csv_separator=",")
class WiredCsvComma(WiredThing):
    search: SeleneElement


URL = "https://ya.ru/"


def test_locators_working_yml(start_stop):
    browser.open_url(URL)
    assert WiredYml().button.text == "Найти"


@pytest.mark.parametrize("wrd_cls", [WiredCsv, WiredCsvComma])
def test_locators_working_csv(start_stop, wrd_cls):
    browser.open_url(URL)
    assert wrd_cls().search.text == "Найти"


def test_fill_field(start_stop):
    browser.open_url(URL)
    page = WiredYml()
    page.input = "google"
    assert page.input.text == "google"


class ButtonList(List[Button]):
    pass


W3C = "https://www.w3schools.com/html/default.asp"


@wired("collections.yml")
class WiredCollection(BaseWidget):
    buttons: List[Button]
    buttons_typed: ButtonList


HOME_NEXT = ["❮ Home", "Next ❯"]


def _home_next_widget():
    return WiredCollection(browser.element("div.nextprev"))


def test_collection_conversion(start_stop):
    browser.open_url(W3C)
    coll = _home_next_widget().buttons
    assert coll
    for button in coll:
        assert isinstance(button, Button)
        assert button.text in HOME_NEXT


def test_collection_conversion_equal(start_stop):
    browser.open_url(W3C)
    t = _home_next_widget()
    assert t.buttons_typed == t.buttons


def test_collection_conversion_inheritance(start_stop):
    browser.open_url(W3C)
    coll = _home_next_widget().buttons_typed
    assert coll
    for button in coll:
        assert isinstance(button, Button)
        assert button.text in HOME_NEXT
