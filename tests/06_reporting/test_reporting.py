from pytest import mark, raises
from selene import browser
from selene.conditions import text, visible

from ocomone_selene.reporting import tags, validation


def test_tags():
    @mark.m1
    @mark.m2
    def test_marked(): ...

    @tags("m1", "m2")
    def test_tagged(): ...

    assert sorted(test_tagged.pytestmark) == sorted(test_marked.pytestmark)


def test_validation(start_stop):
    @validation
    def _fail():
        elem = browser.element("button[type=submit]")
        elem.assure(visible)
        elem.assure(text("Search somewhere else!"))

    browser.open_url("http://ya.ru")
    with raises(AssertionError):
        _fail()
