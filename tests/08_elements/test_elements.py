# -*- coding=utf-8 -*-
import random
from enum import Enum

from selene import browser
from selene.conditions import hidden, in_dom, visible
from selene.elements import SeleneElement

from ocomone_selene.elements import Button, Checkbox, ReadonlyElement, Select, get_element_value
from ocomone_selene.widgets import BaseWidget
from tests.server.api import base, checkbox_result, checkboxes, dropdown_result, dropdowns


class WithSubmit(BaseWidget):
    submit: Button  # id: submit


class DropdownForm(WithSubmit):
    """Page with dropdowns"""

    dropdown: Select  # id: simple-select-1
    dropdown_labeled: Select  # label: "Simple Select!"
    submit: Button  # id: submit


class ResultForm(BaseWidget):
    """Result form"""
    result: ReadonlyElement  # id: result


class StdEnum(Enum):
    OPT1 = "Option number one"
    OPT2 = "Another Option"
    OPT3 = "One More Option"


STD_OPTIONS = {
    "OPT1": StdEnum.OPT1.value,
    "OPT2": StdEnum.OPT2.value,
    "OPT3": StdEnum.OPT3.value,
}


def _prepare_dropdowns():
    browser.open_url(dropdowns(STD_OPTIONS))
    drops = DropdownForm("#filter-container")
    drops.assure(in_dom)
    drops.assure(visible)
    return drops


def _result_should_be(expected):
    result_form = ResultForm()
    assert result_form.result.text == expected


def test_dropdowns(start_stop, with_server):
    drops = _prepare_dropdowns()
    key = random.choice(list(STD_OPTIONS))
    drops.dropdown.select_by_value(key)
    drops.submit.click()
    _result_should_be(dropdown_result(key, STD_OPTIONS[key]))


def test_dropdowns_enum(start_stop, with_server):
    drops = _prepare_dropdowns()
    drops.dropdown = StdEnum.OPT2
    drops.submit.click()
    result_form = ResultForm()
    assert result_form.result.text == dropdown_result(StdEnum.OPT2.name, StdEnum.OPT2.value)


def test_selected_dropdowns(start_stop, with_server):
    drops = _prepare_dropdowns()
    key = random.choice(list(STD_OPTIONS))
    drops.dropdown.select_by_value(key)
    assert get_element_value(drops.dropdown) == key
    assert get_element_value(drops.dropdown) == STD_OPTIONS[key]


def test_select_visible(start_stop, with_server):
    drops = _prepare_dropdowns().dropdown
    drops.assure(visible)
    drops.assure_not(hidden)


def test_dropdown_children(start_stop, with_server):
    drops = _prepare_dropdowns()
    drops.assure(visible)
    options = drops.dropdown.elements("option")
    for opt in options:
        key = opt.get_attribute("value")
        assert key in STD_OPTIONS
        assert opt.text == STD_OPTIONS[key]


CHECKBOXES = ["checkbox", "checkbox2"]


class CheckboxForm(WithSubmit):
    """Checkbox IDs should be same as property names"""

    checkbox: Checkbox  # id: checkbox
    checkbox2: SeleneElement  # id: checkbox2


def _prepare_checkboxes():
    browser.open_url(checkboxes(CHECKBOXES))
    drops = CheckboxForm("#filter-container")
    drops.assure(in_dom)
    drops.assure(visible)
    return drops


def test_checkboxes(start_stop, with_server):
    checkbox_f = _prepare_checkboxes()
    checkbox_f.checkbox.assure(visible)
    checkbox_f.checkbox = True
    form_data = checkbox_f.data()
    checkbox_f.submit.click()
    result_form = ResultForm()
    assert result_form.result.text == checkbox_result(form_data)


def test_checkboxes_widget(start_stop, with_server):
    checkbox_f = _prepare_checkboxes()
    checkbox_f.checkbox.assure(visible)
    checkbox_f.checkbox = True
    checkbox_f.validate_fields(checkbox_f.mirror(checkbox=True, checkbox2=False))


def test_checkboxes_widget_double_select(start_stop, with_server):
    checkbox_f = _prepare_checkboxes()
    checkbox_f.checkbox.assure(visible)
    checkbox_f.checkbox = True
    checkbox_f.checkbox = True
    checkbox_f.validate_fields(checkbox_f.mirror(checkbox=True, checkbox2=False))


def test_sew_displayed(start_stop, with_server):
    checkbox_f = _prepare_checkboxes()
    checkbox_f.checkbox.assure(visible)
    assert checkbox_f.checkbox.is_displayed() is True


def test_sew_displayed_negative(start_stop, with_server):
    checkbox_f = _prepare_checkboxes()
    _prepare_dropdowns()
    assert checkbox_f.checkbox.is_displayed() is False


class HiddenWidget(BaseWidget):
    a: ReadonlyElement  # id: invisible-a

    def __init__(self):
        super().__init__("invisible-div")


def test_sew_assure_not(start_stop, with_server):
    browser.open_url(base())
    widget = HiddenWidget()
    widget.a.assure_not(visible)
