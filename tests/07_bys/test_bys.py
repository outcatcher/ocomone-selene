# -*- coding=utf-8 -*-
import pytest
from ocomone import Resources
from selene import browser
from selene.conditions import enabled, visible

from ocomone_selene import WiredDecorator
from ocomone_selene.elements import TextInput
from ocomone_selene.widgets import BaseWidget


class SuggestionForm(BaseWidget):
    email: TextInput  # label: Your E-mail:


wired = WiredDecorator(Resources(__file__, "."))


@wired("sug.yml")
class SuggerstionWired(BaseWidget):
    email: TextInput


@wired("sug.csv")
class SuggerstionWiredCsv(BaseWidget):
    email: TextInput


W3C = "https://www.w3schools.com/html/default.asp"


@pytest.mark.parametrize("sug_f", [SuggestionForm, SuggerstionWired, SuggerstionWiredCsv])
def test_label_strategy(start_stop, sug_f):
    browser.open_url(W3C)
    browser.element("a.w3-button[onclick*=displayError]").click()
    sug_f = sug_f()
    sug_f.email.assure(visible)
    sug_f.email.assure(enabled)
    assert sug_f.email.tag_name == "input"
