# -*- coding=utf-8 -*-
"""Fixtures and test configurations"""
import os
import socket
import time
from threading import Thread

import pytest
import requests
from selene import browser, config

from ocomone_selene.browser import headless_firefox, remote
from .server.api import http_url
from .server.server import SERVER


def random_port():
    """Get random free port"""
    sock = socket.socket()
    sock.bind(("localhost", 0))
    port = sock.getsockname()[1]
    sock.close()
    return port


def _in_ci():
    return os.getenv("CI_JOB_NAME") is not None


skip_if_ci = pytest.mark.skipif(_in_ci(), reason="Running in CI")


@pytest.fixture(scope="session")
def start_stop():
    """Browser session"""
    if _in_ci():
        _brw = os.getenv("BROWSER")
        print(f"Selected browser is {_brw}")
        remote(_brw)
    else:
        headless_firefox()
    yield
    browser.quit()


def _assure_started(host, port, timeout=60):
    """Check that server is up and running"""
    session = requests.session()
    session.trust_env = False

    def _ok():
        try:
            response = session.get(f"http://{host}:{port}/", timeout=2)
            return response.status_code == 200
        except requests.exceptions.ConnectionError:
            return False

    end_time = time.time() + timeout
    while time.time() < end_time:
        if _ok():
            return
        time.sleep(0.2)
    raise TimeoutError(f"Server is not started in {timeout} seconds")


@pytest.fixture(scope="session")
def with_server():
    """Start local test server in dedicated process"""
    port = random_port()
    _address = socket.gethostbyname(socket.getfqdn()), port
    server_process = Thread(target=SERVER.run, args=_address, daemon=True)
    server_process.start()
    config.base_url = http_url(*_address)
    _assure_started(*_address)
    return
