# -*- coding=utf-8 -*-
from typing import List

from selene import browser
from selene.elements import SeleneElement

from ocomone_selene.comators import AbstractCommentParser
from ocomone_selene.elements import Button
from ocomone_selene.widgets import BaseWidget


class CommentedLocator(AbstractCommentParser):
    def element(self, locator) -> SeleneElement:
        return browser.element(locator)

    search: SeleneElement  # css:button


class ButtonList(List[Button]):
    pass


class CommentedCollection(BaseWidget):
    buttons: List[Button]  # css: a.w3-btn
    buttons_typed: ButtonList  # css: a.w3-btn


def test_locators_working(start_stop):
    browser.open_url("https://ya.ru/")
    assert CommentedLocator().search.text == "Найти"


W3C = "https://www.w3schools.com/html/default.asp"

HOME_NEXT = ["❮ Home", "Next ❯"]


def _home_next_widget():
    return CommentedCollection(browser.element("div.nextprev"))


def test_collection_conversion(start_stop):
    browser.open_url(W3C)
    coll = _home_next_widget().buttons
    assert coll
    for button in coll:
        assert isinstance(button, Button)
        assert button.text in HOME_NEXT


def test_collection_conversion_equal(start_stop):
    browser.open_url(W3C)
    t = _home_next_widget()
    assert t.buttons_typed == t.buttons


def test_collection_conversion_inheritance(start_stop):
    browser.open_url(W3C)
    coll = _home_next_widget().buttons_typed
    assert coll
    for button in coll:
        assert isinstance(button, Button)
        assert button.text in HOME_NEXT
