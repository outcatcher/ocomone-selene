# -*- coding=utf-8 -*-
from selenium.webdriver import Chrome, ChromeOptions, Firefox, FirefoxOptions

from ocomone_selene.browser import browser, headless_chrome, headless_firefox, options
from tests.conftest import skip_if_ci


def test_get_options():
    opt = options("firefox")
    opt_c = options("chrome")
    assert isinstance(opt, FirefoxOptions)
    assert isinstance(opt_c, ChromeOptions)


@skip_if_ci
def test_setup_chrome():
    headless_chrome()
    assert isinstance(browser.driver(), Chrome)


@skip_if_ci
def test_setup_firefox():
    headless_firefox()
    assert isinstance(browser.driver(), Firefox)
