# -*- coding=utf-8 -*-
import os

from ocomone_selene.texts import AbstractTextMapping


class Mapper(AbstractTextMapping):
    """Tested mapping"""

    file_path = os.path.dirname(__file__) + "/mappings.ini"

    MAPPED_TEXT: str
    LOCAL_TEXT: str = "local text"

    CATEGORY_MAPPED_TEXT: str


def test_plain():
    assert Mapper.MAPPED_TEXT == "mapped text"


def test_local():
    assert Mapper.LOCAL_TEXT == "local text"


def test_category():
    assert Mapper.CATEGORY_MAPPED_TEXT == "categorized mapped text"
