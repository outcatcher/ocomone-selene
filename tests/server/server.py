# -*- coding=utf-8 -*-
"""Test server entry point"""
import logging
import os

from flask import Flask, render_template, request
from ocomone import setup_logger

__all__ = ["SERVER"]

BASE_HOST = "localhost"
SERVER = Flask("APP", root_path=os.path.dirname(__file__))

BASE_PORT = 8066

LOGGER = logging.getLogger("tests.server")
setup_logger(LOGGER)


@SERVER.route("/")
def check():
    """Check that server is running"""
    return "OK"


@SERVER.route("/base")
def base_page():
    return render_template("base.html")


@SERVER.route("/dropdowns")
def dropdowns():
    """'Select' elements page"""
    options = request.args["options"]
    options = [opt.split(":") for opt in options.split(",")]
    return render_template("dropdowns.html", options=options)


@SERVER.route("/checkboxes")
def checkboxes():
    options = request.args["options"]
    options = options.split(",")
    return render_template("checkboxes.html", checkboxes=options)


@SERVER.route("/result")
def result():
    """"Shows result page with given text"""
    text = request.args["text"]
    return render_template("result.html", text=text)


if __name__ == '__main__':
    SERVER.run(BASE_HOST, BASE_PORT, debug=True)
