# -*- coding=utf-8 -*-
"""Methods to build server URLs"""

from .server import BASE_HOST, BASE_PORT

BASE_URL = f"http://{BASE_HOST}:{BASE_PORT}"


def dropdowns(options: dict):
    """Generate dropdowns URL"""
    opt_lst = list(options.items())
    opt_str = ",".join(f"{name}:{text}" for name, text in opt_lst)
    url = f"/dropdowns?options={opt_str}"
    return url


def base():
    return f"/base"


def checkboxes(options: list):
    """Generate checkboxes URL"""
    options = ",".join(options)
    return f"/checkboxes?options={options}"


def dropdown_result(key, text):
    """Row for dropdown submit click"""
    return f"{key}:{text}"


def checkbox_result(data: dict):
    return ",".join(f"{key}:{str(value).lower()}" for key, value in data.items())


def http_url(host, port):
    """Returns URL for given host/port"""
    return f"http://{host}:{port}"
