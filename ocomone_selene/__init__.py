"""Selene tweaks and features"""
import logging

from ocomone import setup_logger

from .bys import by_id, by_label
from .comators import AbstractCommentParser
from .wiring import Wireable, WiredDecorator, register_setter, register_strategy, register_without_setter

LOGGER = logging.getLogger(__name__)
setup_logger(LOGGER)
