# -*- coding=utf-8 -*-
"""Library for mapping class with texts to .ini file"""
import os
from configparser import ConfigParser

DEFAULT_CAT = "DEFAULT"


def _f_name(alias, category):
    if category.upper() == DEFAULT_CAT:
        return alias.upper()
    return f"{category.upper()}_{alias.upper()}"


class TextMappingMeta(type):
    """Metaclass for creating set of texts loaded from ini files"""

    def __init__(cls, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not hasattr(cls, "file_path") or not getattr(cls, "file_path"):  # if file_path is missing or empty
            return  # can't map
        file_path: str = getattr(cls, "file_path")
        configs = ConfigParser(interpolation=None)
        if not os.path.isfile(file_path):
            raise TypeError(f"{file_path} is not file")
        configs.read(file_path, "utf8")
        if not configs.sections():
            raise AttributeError(f"File {file_path} is not valid INI file")

        sections = configs.sections()
        sections.append(DEFAULT_CAT)  # DEFAULT category is not included in sections()

        for category in sections:
            messages = configs[category]

            for message in messages:
                f_name = _f_name(message, category)
                if hasattr(cls, f_name):
                    continue  # skip existing field
                setattr(cls, f_name, messages[message])


class AbstractTextMapping(metaclass=TextMappingMeta):
    """Base class text mappings"""

    file_path: str  # required field for mapping
