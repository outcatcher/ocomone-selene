# -*- coding=utf-8 -*-
"""Condition to selene condition wrapper"""
from typing import Any, Callable

from selene.abctypes.conditions import IEntityCondition
from selene.exceptions import ConditionMismatchException


def get_condition(fnc: Callable[[Any], Any], description: str = None) -> IEntityCondition:
    """Create new Selene `condition`

    :param fnc: condition function, that can be applied to some object
    :param description: condition description, shown in error
    """
    if isinstance(fnc, IEntityCondition):
        return fnc

    class _PlainCondition(IEntityCondition):

        def fn(self, entity):
            """'Check' function"""
            res = fnc(entity)
            if not res:
                raise ConditionMismatchException
            return res

        def description(self):
            """Condition description"""
            return description or f"{self}"

    return _PlainCondition()
