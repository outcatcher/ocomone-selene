# -*- coding=utf-8 -*-
"""Map field to element via comment"""
import inspect
import re
from copy import copy
from typing import Type

from ocomone_selene.wiring import (
    Wireable, _STRATEGIES, _cached_getter, GetLocator, _to_property, _wired_getter,
    _add_label_strategy
)

FIELD_RE = re.compile(r"^\s+(\w+?):\s*([\w\[\]]+?)\s+#\s*(.+)$")  # e.g. `field_2: SeleneElement  # id:my-id`


class CommentParsingMeta(type):
    """Metaclass for creating class with locators in comments"""

    def __init__(cls, *args, **kwargs):
        super().__init__(*args, **kwargs)
        lines = inspect.getsourcelines(cls)[0]
        cls.__strategies__ = copy(_STRATEGIES)
        cls.__init__ = _add_label_strategy(cls.__init__)
        for line in lines:
            if "#" not in line:  # if no comment — nothing to do
                continue
            re_found = FIELD_RE.search(line)
            if not re_found:
                continue
            field_name, _, locator = re_found.groups()  # field class text is dropped
            if hasattr(cls, field_name):  # ignore already set fields
                continue
            field_class: Type[Wireable] = cls.__annotations__[field_name]
            locator = GetLocator(*locator.split(":", 1))
            getter = _cached_getter(_wired_getter(field_class, locator), field_name)
            new_property = _to_property(field_class, getter)
            setattr(cls, field_name, new_property)


# noinspection PyAbstractClass
# pylint: disable=abstract-method
class AbstractCommentParser(Wireable, metaclass=CommentParsingMeta):
    """Class containing fields to be parsed"""
