# -*- coding=utf-8 -*-
"""Wrapper for most common used web elements"""
from enum import Enum
from typing import Any, Callable, List, Union

from selene.abctypes.conditions import IEntityCondition
from selene.conditions import in_dom
from selene.elements import SeleneCollection, SeleneElement
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.select import Select as _Select

from .conditions import get_condition
from .wiring import Wireable, register_setter, register_without_setter

ConditionType = Union[IEntityCondition, Callable[[Any], Any]]


# pylint: disable=abstract-method

class SeleneElementWrapper(SeleneElement):
    """Superclass for wrappers around :class:`SeleneElement`"""

    def __init__(self, _element: SeleneElement):
        super().__init__(_element._locator, _element._webdriver)  # pylint: disable=protected-access

    def is_displayed(self):
        """Check that element exists and shown"""
        try:
            self.assure(in_dom, timeout=0)
            return super().is_displayed()
        except TimeoutException:
            return False

    @property
    def text(self):
        """Return visible element text"""
        text = super().text or self.get_attribute("value")
        return text

    def assure(self, condition: ConditionType, timeout=None):
        """Assure that condition is reached"""
        super().assure(get_condition(condition), timeout)

    def assure_not(self, condition: ConditionType, timeout=None):
        """Assure that 'not'-condition is reached"""
        super().assure_not(get_condition(condition), timeout)


class ReadonlyElement(SeleneElementWrapper):
    """Class for non-editable elements"""


register_without_setter(ReadonlyElement)


class Message(ReadonlyElement):
    """Message box wrapper for both error and success message"""


class Button(ReadonlyElement):
    """Button wrapper"""


class TextInput(SeleneElementWrapper):
    """Mutable input wrapper adding setter to text"""

    # noinspection PyMethodOverriding
    @SeleneElementWrapper.text.setter  # pylint: disable=no-member
    def text(self, value: str):
        """Input text into element"""
        self.set(value)


register_setter(TextInput, TextInput.set)


def _select_checkbox(element: SeleneElement, value: bool):
    if element.is_selected() != value:
        element.click()


class Checkbox(SeleneElementWrapper):
    """Checkbox input"""

    def set(self, value: bool):  # pylint: disable=arguments-differ
        """Set checkbox value"""
        _select_checkbox(self, value)


register_setter(Checkbox, Checkbox.set)


class Select(_Select, Wireable):
    """Wrapper around """
    _el: SeleneElement

    def __init__(self, element: SeleneElement):
        _Select.__init__(self, element)
        self._el = element

    def elements(self, ccs_selector_or_by) -> SeleneCollection:
        return self._el.elements(ccs_selector_or_by)

    def element(self, ccs_selector_or_by) -> SeleneElement:
        return self._el.element(ccs_selector_or_by)

    def select_by_value(self, value: Union[Enum, str]):
        """Select by value equals given value or value.name if value is :class:`Enum`"""
        if isinstance(value, Enum):
            value = value.name
        super().select_by_value(value)

    def assure(self, condition, timeout=None):
        """Assure method of SeleneElement"""
        self._el.assure(condition, timeout)

    def assure_not(self, condition, timeout=None):
        """Assure not method of SeleneElement"""
        self._el.assure_not(condition, timeout)


register_setter(Select, Select.select_by_value)

_ValueList = List[Union[Enum, str]]


class MultiSelect(Select):
    """Select accepting list of values"""

    def select_by_value(self, values: _ValueList):
        """Select several values"""
        if not isinstance(values, list):
            values: _ValueList = [values]
        for value in values:
            super().select_by_value(value)


def __is_checkbox(element: SeleneElement):
    if isinstance(element, Checkbox):
        return True
    if (element.tag_name == "input") and (element.get_attribute("type") == "checkbox"):
        return True
    return False


class Selected(list):
    """List of selected options"""

    def __init__(self, select_element: Select, single=False):

        def _val(elem):
            return elem.get_attribute("value")

        if not single:
            selected = [(_val(element), element.text) for element in select_element.all_selected_options]
        else:
            element: WebElement = select_element.first_selected_option
            selected = [(_val(element), element.text)]
        super().__init__(selected)

    def __eq__(self, other):
        if not isinstance(other, list):
            val, text = self[0]
            return (val == other) or (text == other)
        values = []
        texts = []
        for elem in self:
            values.append(elem[0])
            texts.append(elem[1])
        return (values == other) or (texts == other)

    def __ne__(self, other):
        return not self.__eq__(other)


def get_element_value(element: Union[SeleneElementWrapper, SeleneElement, Checkbox, Select]) -> Union[str, list]:
    """Get value of web element"""
    if isinstance(element, MultiSelect):
        return Selected(element, single=False)
    if isinstance(element, _Select):
        return Selected(element, single=True)
    if __is_checkbox(element):
        return element.is_selected()
    return element.text
