# -*- coding=utf-8 -*-
"""Base class for creating Wireable widgets"""

import logging
from collections import namedtuple
from typing import Any, Dict, Tuple, Type, Union

import allure
import yaml
from allure_commons.types import AttachmentType
from selene import browser
from selene.elements import SeleneElement
from selenium.common.exceptions import WebDriverException

from .comators import CommentParsingMeta
from .elements import ReadonlyElement, get_element_value
from .wiring import Wireable, get_custom_element, register_setter

__all__ = ["BaseWidget"]

LOGGER = logging.getLogger(__name__)


class NamedTupleAbstract(tuple):
    """Type hint stub for ``collections.namedtuple``"""
    _fields: tuple
    _fields_defaults: dict

    # this is stub for typing
    def _asdict(self) -> dict: ...  # pylint: disable=pointless-statement,multiple-statements,no-self-use


EMPTY_MIRROR = namedtuple(f"EmptyMirror", [])


def _class_mirror(cls):
    """Data structure for given class"""

    if not hasattr(cls, "__annotations__"):
        return EMPTY_MIRROR
    fields = [fld for fld in cls.__annotations__.keys() if not fld.startswith("_")]  # pylint: disable=no-member
    defaults = [None for _ in fields]
    result = namedtuple(f"Mirror{cls.__name__}", fields, defaults=defaults)
    result.__eq__ = lambda x, y: not _diff_ignoring_missing(x, y)
    result.__ne__ = _diff_ignoring_missing
    result.__repr__ = lambda self: repr(__to_dict(self))
    return result


class WidgetMeta(CommentParsingMeta):
    """Auto-registering widgets"""

    def __init__(cls: Type["BaseWidget"], *args, **kwargs):
        super().__init__(*args, **kwargs)

        if cls.__name__ != "BaseWidget":
            register_setter(cls, cls.fill)

        cls.mirror = _class_mirror(cls)


def __to_dict(obj):
    if hasattr(obj, "_fields"):  # looks like named tuple
        return dict(obj._asdict())
    return obj


PackedData = Union[NamedTupleAbstract, dict]


def _diff_ignoring_missing(obj: PackedData, other: PackedData) -> Dict[str, Tuple[Any, Any]]:
    obj_dict = __to_dict(obj)
    other_dict = __to_dict(other)
    diff = {}
    for key, val in obj_dict.items():
        if val is None:
            continue
        other_val = other_dict.get(key, None)
        if other_val is None:
            continue
        if val != other_val:
            diff[key] = (val, other_val)
    return diff


# noinspection PyProtectedMember
class BaseWidget(Wireable, metaclass=WidgetMeta):
    """Base class for widgets"""

    def __init__(self, root_element: Union[ReadonlyElement, Tuple[str, str], str] = None):
        if root_element is None:
            root_element = "html"
        if isinstance(root_element, tuple) or isinstance(root_element, str):  # if `root_element` is locator
            self._root_element_locator = root_element
        elif isinstance(root_element, SeleneElement):
            self._root_element_locator = root_element._locator._by

    @property
    def root_element(self):
        """Get root element"""
        return browser.element(self._root_element_locator)

    def get_element(self, field_name, field_type=SeleneElement, type_args: tuple = ()):
        """Get locator for given field name"""
        if hasattr(self, field_name):
            return getattr(self, field_name)
        return get_custom_element(self, field_name, field_type, type_args)

    def is_displayed(self) -> bool:
        """Shows that root element is visible"""
        return self.root_element.is_displayed()

    def assure(self, condition, timeout=None):
        """Wait for timeout seconds for condition to become True"""
        return self.root_element.assure(condition, timeout)

    def assure_not(self, condition, timeout=None):
        """Wait for timeout seconds for condition to become False"""
        return self.root_element.assure_not(condition, timeout)

    def element(self, ccs_selector_or_by):
        """Find element if widget"""
        return self.root_element.element(ccs_selector_or_by)

    def elements(self, ccs_selector_or_by):
        """Find element if widget"""
        return self.root_element.elements(ccs_selector_or_by)

    def __eq__(self, other):
        return not self.__ne__(other)

    def __ne__(self, other):
        if isinstance(other, BaseWidget):
            other = other.data()
        this = self.data()
        return _diff_ignoring_missing(this, other)

    @allure.step("Fill All Fields With {mirror_data}")
    def fill(self, mirror_data: Union[NamedTupleAbstract, dict]) -> None:
        """Fill fields with mirror info

        This is default setter for ``Widget`` instances"""

        if hasattr(mirror_data, "_fields"):  # is named tuple
            mirror_data = mirror_data._asdict()
        for field in mirror_data:
            value = mirror_data[field]
            if not hasattr(self, field):
                LOGGER.warning("Trying to set field '%s' for %s missing this field", field, self)
                continue
            try:
                setattr(self, field, value)
            except WebDriverException as exc:
                allure.attach.file(browser.take_screenshot(),
                                   name="InvalidElementState", attachment_type=AttachmentType.PNG)
                LOGGER.exception("Can't set field %s of type %s", field, type(getattr(self, field)))
                raise exc

    def __get_child_data(self, field):
        value = getattr(self, field)
        if isinstance(value, ReadonlyElement):
            return None
        if isinstance(value, BaseWidget):
            value = value.data()
        else:
            value = get_element_value(value)
        return value

    def data(self) -> dict:
        """Copy widget data to the mirror"""
        fields = {}
        for field in self.mirror._fields:
            try:
                value = self.__get_child_data(field)
            except WebDriverException as wde:  # just skip fields we can't get
                LOGGER.warning("Can't get value of field `%s`", field)
                LOGGER.debug("Exception was raised: %s", wde)
                continue
            if value is not None:
                fields[field] = value
        return fields

    # noinspection PyShadowingBuiltins
    def load(self, file_name: str):
        """Load data from file into widget fields"""
        with open(file_name) as file:
            data = yaml.load(file)
        self.fill(data)

    def dump(self, file_name: str):
        """Dump data to yml file"""
        with open(file_name, "w+") as file:
            data = self.data()
            yaml.dump(data, file, default_flow_style=False)

    @allure.step("Validate All Fields")
    def validate_fields(self, expected_data: Union[NamedTupleAbstract, dict]) -> None:
        """Check that all fields are same as given in file

        Raises assertion error if some fields are out of sync.

        Logs diff as error.
        """
        data = self.data()
        diff = _diff_ignoring_missing(data, expected_data)
        if diff:
            text_diff = "\n".join(
                f"Field `{field}` differs. Expected: {exp} != {act}" for field, (exp, act) in diff.items())
            raise AssertionError(f"Expected and actual data is different: \n{text_diff}")
