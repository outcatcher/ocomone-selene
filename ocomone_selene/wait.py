# -*- coding=utf-8 -*-
"""Waiting functions"""
import time
from typing import Callable

from ocomone_selene.elements import SeleneElementWrapper


def wait_for(fnc: Callable[..., bool], *args, timeout=5.0, message: str = None, assertion=False, polling=0.05):
    """Wait for condition to be fulfilled

    :param fnc: condition to wait for
    :param args: condition arguments
    :param timeout: wait timeout
    :param message: custom timeout message, overriding default
    :param assertion: change TimeoutError to AssertionError raised on timeout
    :param polling: retry timeout
    """
    end_time = time.time() + timeout
    while time.time() < end_time:
        if fnc(*args):
            return
        time.sleep(polling)
    message = message or f"Condition {fnc} not reached for arguments {args} in {timeout} seconds"
    if assertion:
        raise AssertionError(message)
    raise TimeoutError(message)


def wait_until_element_is_shown(obj: SeleneElementWrapper, timeout=5):
    """Wait until given element is shown"""
    wait_for(obj.is_displayed, timeout=timeout)


def wait_until_element_is_hidden(obj: SeleneElementWrapper, timeout=5):
    """Wait until given element is hidden"""
    wait_for(lambda: not obj.is_displayed(), timeout=timeout)
