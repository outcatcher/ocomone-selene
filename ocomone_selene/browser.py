# -*- coding=utf-8 -*-
"""Features for selene browser management"""
import os
from typing import Union

from selene import browser, config
from selene.browsers import BrowserName
from selenium import webdriver
from selenium.webdriver import ChromeOptions, FirefoxOptions


def options(browser_name) -> Union[ChromeOptions, FirefoxOptions]:
    """Get options for given browser"""
    if browser_name == BrowserName.CHROME:
        return ChromeOptions()
    if browser_name == BrowserName.FIREFOX:
        return FirefoxOptions()


def _headless_browser(browser_name: BrowserName):
    if browser_name == BrowserName.CHROME:
        from webdriver_manager.chrome import ChromeDriverManager as Manager
        drv_cls = webdriver.Chrome
    elif browser_name == BrowserName.FIREFOX:
        from webdriver_manager.firefox import GeckoDriverManager as Manager
        drv_cls = webdriver.Firefox
    else:
        raise AttributeError(f"Invalid browser: {browser_name}")
    opts = options(browser_name)
    opts.headless = True
    opts.add_argument("no-sandbox")
    driver = drv_cls(executable_path=Manager().install(), options=opts)
    driver.set_window_size(1920, 1080)
    browser.set_driver(driver)


def headless_chrome():
    """Configure system to use headless chrome"""
    _headless_browser(BrowserName.CHROME)


def headless_firefox():
    """Configure system to use headless firefox"""
    _headless_browser(BrowserName.FIREFOX)


def headless_default():
    """Configure default browser to be headless"""
    browser_name = config.browser_name
    _headless_browser(browser_name)


def remote(browser_name):
    """User remote web driver"""
    host = os.getenv("HOST", "localhost")
    driver = webdriver.Remote(f"http://{host}:4444/wd/hub", options=options(browser_name))
    browser.set_driver(driver)
